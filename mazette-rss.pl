#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::Collection 'c';
use Mojo::DOM;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(encode decode);
use XML::RSS;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
   $year += 1900;
my $month = sprintf("%02d", ++$mon);
my $url   = 'https://mazette.media';

my $login = $ENV{MAZETTE_LOGIN};
my $pwd   = $ENV{MAZETTE_PWD};
my $file  = $ENV{MAZETTE_RSSFILE};

die 'Please set MAZETTE_LOGIN, MAZETTE_PWD and MAZETTE_RSSFILE. Aborting.' unless ($login && $pwd && $file);

my $ua   = Mojo::UserAgent->new();
my $feed = XML::RSS->new(version => '2.0');
$feed->channel(
    title       => 'Mazette ! Les actus',
    link        => $url,
    description => 'Les actus de Mazette du mois courant',
    language    => 'fr',
    generator   => 'mazette-rss'
);

# Connection
my $res = $ua->get("$url/wp-login.php/")->result;
my $form = Mojo::DOM->new($res->body)->find('#loginform')->first;
my $args = {
    log         => $login,
    pwd         => $pwd,
    'wp-submit' => 'Se+connecter',
    redirect_to => "$url/wp-admin",
    testcookie  => 1
};
$ua->post("$url/wp-login.php" => form => $args);

# Get images
my $dom = Mojo::DOM->new(decode('UTF-8', $ua->get("$url/actu/")->result->body));
$dom->find('.swiper-slide')->each(sub {
    my ($e, $num) = @_;

    my $author = $e->find('h5.mazette-font')->first->text;
    my $img    = $e->find('img.swiper-lazy')->first->attr('src');

    my %hash = (
        title       => $author,
        link        => $img,
        description => sprintf('<p>Par %s</p><div><img src="%s">', $author, $img),
        permaLink   => $img,
    );
    $feed->add_item(%hash);
});

$feed->save($file);
